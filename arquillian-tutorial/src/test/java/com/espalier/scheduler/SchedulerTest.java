package com.espalier.scheduler;

import javax.enterprise.inject.spi.Extension;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

// Scheduler Test 3
@RunWith(Arquillian.class)
public class SchedulerTest {

	@Deployment
	public static JavaArchive createDeployment() {
		JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage("com.espalier.scheduler")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsServiceProvider(Extension.class, QuartzExtension.class);
		System.out.println(jar.toString(true));
		return jar;
	}

	@Test
	public void test() throws Exception {
		System.out.println("Sleeping");
		Thread.sleep(10000);
	}
}
