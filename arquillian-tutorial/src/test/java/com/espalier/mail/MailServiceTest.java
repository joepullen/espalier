package com.espalier.mail;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.descriptor.api.Descriptors;
import org.jboss.shrinkwrap.descriptor.api.beans10.BeansDescriptor;
import org.jboss.shrinkwrap.descriptor.api.persistence20.PersistenceDescriptor;
import org.jboss.shrinkwrap.descriptor.api.persistence20.PersistenceUnitTransactionType;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class MailServiceTest {

	public void userSettingChanged(@Observes UserSettingChanged usc) {
		System.out.println("userSettingChanged: " + usc.getUserName());
	}

	@Inject
	MailService mail;

	@Deployment
	public static JavaArchive createDeployment() {

		BeansDescriptor beans = Descriptors.create(BeansDescriptor.class).createInterceptors()
				.clazz("com.espalier.mail.MailInterceptor").up().createDecorators().clazz("com.espalier.mail.UserDecorator").up();

		PersistenceDescriptor persistence = Descriptors.create(PersistenceDescriptor.class).version("2.0")
				.createPersistenceUnit().name("customerDB")
				.transactionType(PersistenceUnitTransactionType._RESOURCE_LOCAL)
				.provider("org.eclipse.persistence.jpa.PersistenceProvider").clazz("com.espalier.mail.User")
				.getOrCreateProperties().createProperty().name("javax.persistence.jdbc.driver")
				.value("oracle.jdbc.driver.OracleDriver").up().createProperty().name("javax.persistence.jdbc.url")
				.value("jdbc:oracle:thin:@localhost:1521:XE").up().createProperty().name("javax.persistence.jdbc.user")
				.value("ODS").up().createProperty().name("javax.persistence.jdbc.password").value("peterpan").up()
				.createProperty().name("eclipselink.ddl-generation").value("drop-and-create-tables").up()
				.createProperty().name("eclipselink.logging.level.sql").value("FINE").up().createProperty()
				.name("eclipselink.logging.parameters").value("true").up().up().up().createPersistenceUnit()
				.name("adminDB").transactionType(PersistenceUnitTransactionType._RESOURCE_LOCAL)
				.provider("org.eclipse.persistence.jpa.PersistenceProvider").getOrCreateProperties().createProperty()
				.name("javax.persistence.jdbc.driver").value("oracle.jdbc.driver.OracleDriver").up().createProperty()
				.name("javax.persistence.jdbc.url").value("jdbc:oracle:thin:@localhost:1521:XE").up().createProperty()
				.name("javax.persistence.jdbc.user").value("ODS").up().createProperty()
				.name("javax.persistence.jdbc.password").value("peterpan").up().up().up();

		System.out.println(persistence.exportAsString());

		JavaArchive jar = ShrinkWrap.create(JavaArchive.class).addPackage("com.espalier.mail")
				.addAsManifestResource(new StringAsset(persistence.exportAsString()), "persistence.xml")
				.addAsManifestResource(new StringAsset(beans.exportAsString()), "beans.xml");
		System.out.println(jar.toString(true));
		return jar;
	}

	@Test
	public void test() {
		mail.send("from", "to", "body");
		mail.listUsers();
	}
}
