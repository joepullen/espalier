package com.espalier.scheduler;

import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CDIJob implements Job {

	public static final String JOB_CLASS_NAME = "CDI_JOB_CLASS_NAME";

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobData = context.getJobDetail().getJobDataMap();
		String className = jobData.getString(JOB_CLASS_NAME);

		Class<? extends Runnable> jobClass;

		try {
			jobClass = Class.forName(className).asSubclass(Runnable.class);
		} catch (ClassNotFoundException bad) {
			throw new JobExecutionException(bad);
		}

		BeanManager bm = QuartzExtension.getBeanManager();
		Set<Bean<?>> jobBeans = bm.getBeans(jobClass);
		@SuppressWarnings("unchecked")
		Bean<Runnable> jobBean = (Bean<Runnable>) bm.resolve(jobBeans);

		CreationalContext<Runnable> c = bm.createCreationalContext(jobBean);
		Runnable job = (Runnable) bm.getReference(jobBean, Runnable.class, c);

		try {
			job.run();
		} finally {
			jobBean.destroy(job, c);
		}
	}
}
