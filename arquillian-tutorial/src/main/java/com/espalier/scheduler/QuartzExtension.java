package com.espalier.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.BeforeBeanDiscovery;
import javax.enterprise.inject.spi.BeforeShutdown;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessAnnotatedType;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class QuartzExtension implements Extension {

	private static BeanManager beanManager;
	private Scheduler scheduler;

	public static BeanManager getBeanManager() {
		return beanManager;
	}

	public void initScheduler(@Observes
	BeforeBeanDiscovery event) throws SchedulerException {
		scheduler = StdSchedulerFactory.getDefaultScheduler();
	}

	public void startScheduler(@Observes
	AfterDeploymentValidation event, BeanManager bm) {
		beanManager = bm;
		try {
			scheduler.start();
			System.out.println("Started");
		} catch (SchedulerException bad) {
			throw new RuntimeException(bad);
		}
	}

	public void shutdownScheduler(@Observes
	BeforeShutdown event) {
		try {
			scheduler.shutdown(true);
		} catch (SchedulerException bad) {
			throw new RuntimeException(bad);
		}
	}

	public void scheduleJob(@Observes
	ProcessAnnotatedType<?> pat) {
		AnnotatedType<?> t = pat.getAnnotatedType();
		Scheduled schedule = t.getAnnotation(Scheduled.class);

		if (schedule == null) {
			return;
		}

		Class<? extends Runnable> jobClass = t.getJavaClass().asSubclass(Runnable.class);

		if (jobClass == null) {
			System.out.println("Cannot schedule job " + t);
			return;
		}

		JobDetail job = newJob(CDIJob.class).usingJobData(CDIJob.JOB_CLASS_NAME, jobClass.getName()).build();
		Trigger trigger = newTrigger().withSchedule(cronSchedule(schedule.value())).build();

		try {
			scheduler.scheduleJob(job, trigger);
			System.out.println("Scheduled Job " + job);
		} catch (SchedulerException bad) {
			throw new RuntimeException(bad);
		}
	}
}
