package com.espalier.scheduler;

import javax.inject.Inject;

@Scheduled("0-59 * * * * ?")
public class MyJob implements Runnable {
	
	@Inject
	private MyService service;
	
	@Override
	public void run() {
		service.doSomething();
	}
}
