package com.espalier.mail;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity()
@Table(name = "CDI_USER")
public class User implements Serializable {
	private static final long serialVersionUID = -1360064250129985841L;

	private Long id;
	private String name;

	public User() {
	}

	public User(String name) {
		this.name = name;
	}

	@Id
	@SequenceGenerator(name = "CDI_USER_ID_SEQ", sequenceName = "CDI_USER_ID_SEQ", initialValue = 1, allocationSize = 10000)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CDI_USER_ID_SEQ")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Size(min = 3, max = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User@" + hashCode() + "[id = " + id + "; name = " + name + "]";
	}
}