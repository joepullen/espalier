package com.espalier.mail;

import java.util.List;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

public class MyMailService implements MailService {

	private @Inject
	ConfigurationService configuration;

	private @Inject
	Event<UserSettingChanged> userChanged;

	private @Inject
	@CustomerDB
	EntityManager emCustomer;

	private @Inject
	@AdminDB
	EntityManager emAdmin;

	@Override
	@Transactional
	public void send(String from, String to, String body) {
		String replyTo = configuration.getReplyToAddress();
		emCustomer.getTransaction().begin();
		User user = new User(replyTo);
		emCustomer.persist(user);
		userChanged.fire(new UserSettingChanged(user.getName()));
		emCustomer.getTransaction().commit();
	}

	@Override
	@Transactional(qualifier=CustomerDB.class)
	public List<User> listUsers() {
		String select = "select u from User u order by u.id";
		System.out.println("Selecting (using JPQL)...");
		List<User> users = emCustomer.createQuery(select, User.class).getResultList();
		return users;
	}
}
