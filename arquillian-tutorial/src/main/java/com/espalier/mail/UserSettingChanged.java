package com.espalier.mail;

public class UserSettingChanged {
	private String userName;

	public UserSettingChanged(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
