package com.espalier.mail;

import java.util.List;

public interface MailService {
	void send(String from, String to, String body);
	List<User> listUsers();
}
