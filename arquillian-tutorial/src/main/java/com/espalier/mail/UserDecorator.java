package com.espalier.mail;

import java.util.List;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

@Decorator
public abstract class UserDecorator implements MailService {

	@Inject
	@Delegate
	@Any
	MailService service;

	@Override
	public List<User> listUsers() {
		List<User> users = service.listUsers();
		System.out.println("UserDecorator Found " + users.size() + " users");
		return users;
	}
}
