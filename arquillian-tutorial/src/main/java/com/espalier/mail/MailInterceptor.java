package com.espalier.mail;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.annotation.Resource;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.UserTransaction;

import com.espalier.mail.tx.AnyLiteral;

@Interceptor
@Transactional
public class MailInterceptor {

	private @Inject
	@Resource
	UserTransaction tx;

	@Inject
	private BeanManager beanManager;

	@SuppressWarnings("unchecked")
	@AroundInvoke
	public Object invoke(InvocationContext context) throws Exception {

		if (beanManager != null) {

			Transactional transactionalAnnotation = context.getMethod().getAnnotation(Transactional.class);

			if (transactionalAnnotation == null) {
				transactionalAnnotation = context.getTarget().getClass().getAnnotation(Transactional.class);
			}

			Class<? extends Annotation> qualifierClass = Default.class;
			if (transactionalAnnotation != null) {
				qualifierClass = transactionalAnnotation.qualifier();
			}

			Set<Bean<?>> entityManagerBeans = beanManager.getBeans(EntityManager.class, new AnyLiteral());
			EntityManager entityManager = null;

			if (entityManagerBeans != null) {
				Bean<EntityManager> entityManagerBean = null;

				loop: for (Bean<?> currentEntityManagerBean : entityManagerBeans) {
					System.out.println(currentEntityManagerBean);
					Set<Annotation> foundQualifierAnnotations = currentEntityManagerBean.getQualifiers();
					for (Annotation currentQualifierAnnotation : foundQualifierAnnotations) {
						if (currentQualifierAnnotation.annotationType().equals(qualifierClass)) {
							entityManagerBean = (Bean<EntityManager>) currentEntityManagerBean;
							break loop;
						}
					}
				}

				entityManager = (EntityManager) beanManager.getReference(entityManagerBean, EntityManager.class,
						beanManager.createCreationalContext(entityManagerBean));
			}

			if (entityManager != null) {
				EntityTransaction eTx = entityManager.getTransaction();

				try {
					if (!eTx.isActive()) {
						eTx.begin();
					}
					return context.proceed();
				} catch (Exception bad) {
					if (eTx.isActive()) {
						eTx.rollback();
					}
					throw bad;
				} finally {
					if (eTx.isActive()) {
						eTx.commit();
					}
				}
			}
		}

		if (tx != null) {
			try {
				tx.begin();
				return context.proceed();
			} catch (Exception bad) {
				throw bad;
			} finally {
				tx.commit();
			}
		}

		return context.proceed();
	}

}
