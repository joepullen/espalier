package com.espalier.mail;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EntityManagerProducers {
	@Produces
	@RequestScoped
	@CustomerDB
	public EntityManager createCustomerDBEM() {
		return Persistence.createEntityManagerFactory("customerDB").createEntityManager();
	}

	@Produces
	@RequestScoped
	@AdminDB
	public EntityManager createAdminDBEM() {
		return Persistence.createEntityManagerFactory("adminDB").createEntityManager();
	}

	public void disposeCustomerEM(@Disposes @CustomerDB EntityManager em) {
		em.close();
	}

	public void disposeAdminEM(@Disposes @AdminDB EntityManager em) {
		em.close();
	}
}
