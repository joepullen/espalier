package com.espalier.mail.tx;

import javax.persistence.EntityManager;

class EntityManagerEntry {
	private EntityManager entityManager;
	private PersistenceContextMetaEntry persistenceContextEntry;

	EntityManagerEntry(EntityManager entityManager, PersistenceContextMetaEntry persistenceContextEntry) {
		this.entityManager = entityManager;
		this.persistenceContextEntry = persistenceContextEntry;
	}

	EntityManager getEntityManager() {
		return entityManager;
	}

	PersistenceContextMetaEntry getPersistenceContextEntry() {
		return persistenceContextEntry;
	}
}
