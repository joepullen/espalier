package com.espalier.mail.tx;

import javax.enterprise.inject.Any;
import javax.enterprise.util.AnnotationLiteral;

public class AnyLiteral extends AnnotationLiteral<Any> implements Any {
	private static final long serialVersionUID = -8623640277155878656L;
}