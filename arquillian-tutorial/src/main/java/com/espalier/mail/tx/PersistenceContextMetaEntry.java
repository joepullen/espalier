package com.espalier.mail.tx;

public class PersistenceContextMetaEntry {
	private Class<?> sourceClass;
	private String fieldName;

	private String unitName;
	private boolean extended;

	PersistenceContextMetaEntry(Class<?> sourceClass, String fieldName, String unitName, boolean extended) {
		this.sourceClass = sourceClass;
		this.fieldName = fieldName;
		this.unitName = unitName;
		this.extended = extended;
	}

	public Class<?> getSourceClass() {
		return sourceClass;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getUnitName() {
		return unitName;
	}

	public boolean isExtended() {
		return extended;
	}
}
